﻿
using UnityEngine;

public class End : MonoBehaviour
{
	public GameMode gameMode;

	void OnTriggerEnter()
	{
		gameMode.CompleteLevel();
	}
}
